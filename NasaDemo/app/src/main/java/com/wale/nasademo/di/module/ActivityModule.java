package com.wale.nasademo.di.module;

import com.wale.nasademo.ui.activities.DashboardActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityModule {

  @ContributesAndroidInjector(modules = FragmentsModule.class)
  abstract DashboardActivity contributesDashboardActivity();

}
