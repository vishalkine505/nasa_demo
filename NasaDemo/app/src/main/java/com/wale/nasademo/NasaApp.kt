package com.wale.nasademo


import com.wale.nasademo.di.AppComponent
import com.wale.nasademo.di.DaggerAppComponent
import com.wale.nasademo.utils.ModelPreferencesManager
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class NasaApp : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent: AppComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

    override fun onCreate() {
        super.onCreate()
        ModelPreferencesManager.with(this)
    }
}