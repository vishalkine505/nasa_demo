package com.wale.nasademo.di.module;

import android.app.Application;
import android.content.Context;
import com.wale.nasademo.api.NasaService;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AppModule {

  @Provides
  @Singleton
  NasaService provideApiService(Retrofit retrofit) {
    return retrofit.create(NasaService.class);
  }

  @Provides
  @Singleton
  Context provideContext(Application application) {
    return application;
  }
}
