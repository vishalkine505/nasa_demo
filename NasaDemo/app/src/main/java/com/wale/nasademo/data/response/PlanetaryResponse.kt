package com.wale.nasademo.data.response

data class PlanetaryResponse(
    val date: String,
    val explanation: String,
    val media_type: String,
    val thumbnail_url: String,
    val service_version: String,
    val title: String,
    val url: String
)