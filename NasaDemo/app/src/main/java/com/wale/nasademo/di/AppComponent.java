package com.wale.nasademo.di;

import android.app.Application;


import com.wale.nasademo.di.module.ActivityModule;
import com.wale.nasademo.di.module.AppModule;
import com.wale.nasademo.di.module.FragmentsModule;
import com.wale.nasademo.di.module.NetworkModule;
import com.wale.nasademo.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(
    modules = {
      AndroidSupportInjectionModule.class,
      AppModule.class,
      NetworkModule.class,
      ViewModelModule.class,
      ActivityModule.class,
      FragmentsModule.class
    })
public interface AppComponent extends AndroidInjector<DaggerApplication> {
  @Component.Builder
  interface Builder {

    AppComponent build();

    @BindsInstance
    AppComponent.Builder application(Application application);
  }


}
