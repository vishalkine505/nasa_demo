package com.wale.nasademo.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wale.nasademo.R
import com.wale.nasademo.extentions.addFragment
import com.wale.nasademo.ui.fragments.DashboardFragment
import dagger.android.support.DaggerAppCompatActivity

class DashboardActivity : DaggerAppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(DashboardFragment(),R.id.main_content)
    }
}