package com.wale.nasademo.extentions

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.wale.nasademo.utils.AnimationListener


inline fun <T: FragmentActivity>T.addFragment(
    fragment: Fragment, @IdRes containerId: Int,
    tag: String? = null, backStackTag: String? = null
) {
    supportFragmentManager.inTransaction {
        if (tag != null && backStackTag != null){
            add(containerId, fragment, tag)
            addToBackStack(backStackTag)
        }else if(tag != null){
            add(containerId, fragment, tag)
        } else add(containerId, fragment)
    }
}

inline fun FragmentManager.inTransaction(function: FragmentTransaction.() -> FragmentTransaction){
    val ft = beginTransaction()
    ft.function()
    ft.commit()
}
