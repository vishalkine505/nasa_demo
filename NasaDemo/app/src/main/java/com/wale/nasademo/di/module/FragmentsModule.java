package com.wale.nasademo.di.module;

import com.wale.nasademo.ui.fragments.DashboardFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentsModule {

  @ContributesAndroidInjector
  abstract DashboardFragment contributesDashboardFragment();
}
