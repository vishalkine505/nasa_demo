package com.wale.nasademo.api;


import com.wale.nasademo.data.response.PlanetaryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/** REST end points */
public interface NasaService {

  @GET("/planetary/apod?api_key=MhRGqsmJBFluZLXRh5B2skna9tX1aY799Uyaowhz&thumbs=true")
  Call<PlanetaryResponse> getPlanetaryApod();

}
