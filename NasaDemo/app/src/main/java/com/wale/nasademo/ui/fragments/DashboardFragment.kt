package com.wale.nasademo.ui.fragments

import android.os.Bundle
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.wale.nasademo.R
import com.wale.nasademo.common.Constants
import com.wale.nasademo.data.response.PlanetaryResponse
import com.wale.nasademo.databinding.DashboardFragmentBinding
import com.wale.nasademo.di.NasaViewModelFactory
import com.wale.nasademo.extentions.isConnected
import com.wale.nasademo.ui.viewmodels.HomeViewModel
import com.wale.nasademo.utils.ModelPreferencesManager
import dagger.android.support.DaggerFragment
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class DashboardFragment : DaggerFragment() {

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var dashboardBinding: DashboardFragmentBinding

    private lateinit var viewModel: HomeViewModel

    @Inject
    lateinit var factory: NasaViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardBinding =
            DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false);
        dashboardBinding.lifecycleOwner = viewLifecycleOwner
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        dashboardBinding.viewmodel = viewModel
        if (context?.isConnected()!!) {
            viewModel.fetchPlanetary()
        } else {
            val apodData = ModelPreferencesManager.get<PlanetaryResponse>(Constants.apod_data_key)
            apodData?.let {
                setData(it)

                val myDate = ModelPreferencesManager.get<Long>(Constants.apod_time_key)
                    ?.let { it1 -> Date(it1) }
                if (Date().date > myDate?.date!!) {
                    showConnectionError()
                }
            }

        }
        viewModel.planetaryResponse?.observe(viewLifecycleOwner, Observer {
            it?.let {
                setData(it)
                ModelPreferencesManager.put(it, Constants.apod_data_key)
                ModelPreferencesManager.put(
                    Date(System.currentTimeMillis()).time,
                    Constants.apod_time_key
                )
            }
        })

        return dashboardBinding.root
    }

    private fun setData(it: PlanetaryResponse) {
        viewModel.title.set(it.title)
        viewModel.date.set(it.date)
        viewModel.explaination.set(it.explanation)
        if (it.media_type == "video") {
            viewModel.url.set(it.thumbnail_url)
        } else {
            viewModel.url.set(it.url)
        }
    }

    private fun showConnectionError() {
        Toast.makeText(context, getString(R.string.connection_error), Toast.LENGTH_LONG).show()
    }
}