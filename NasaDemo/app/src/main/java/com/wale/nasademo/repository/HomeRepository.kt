package com.demo.fitternity.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wale.nasademo.extentions.onEnqueue
import com.wale.nasademo.api.NasaService
import com.wale.nasademo.data.response.PlanetaryResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepository@Inject constructor(val service: NasaService) {
    companion object {
        private val TAG = this.javaClass.name
    }


    private val planetaryResponse: MutableLiveData<PlanetaryResponse> = MutableLiveData()

    public fun getPlanetaryResponse():LiveData<PlanetaryResponse>{
        return planetaryResponse;
    }

     fun fetchPlanetaryData():MutableLiveData<PlanetaryResponse> {
        service.planetaryApod.onEnqueue {
            onResponse ={
                Log.i(TAG,it.body().toString())
                 planetaryResponse.postValue(it.body())

            }
            onFailure ={
                Log.i(TAG,it?.message.toString())
                planetaryResponse.postValue(null)
            }
        }
        return planetaryResponse
    }



}