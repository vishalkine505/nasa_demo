package com.wale.nasademo.utils

interface AnimationListener {
    fun onAnimationEnd()
}