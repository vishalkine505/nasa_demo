package com.wale.nasademo.ui.viewmodels


import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.fitternity.repository.HomeRepository
import com.squareup.picasso.Picasso
import com.wale.nasademo.R
import com.wale.nasademo.data.response.PlanetaryResponse
import javax.inject.Inject


class HomeViewModel @Inject constructor(var homeRespository: HomeRepository?) : ViewModel() {

     var title = ObservableField<String>()
    var date = ObservableField<String>()
    var explaination = ObservableField<String>()
    var url = ObservableField<String>()


    companion object {
        private val TAG = HomeViewModel::class.java.simpleName

        @JvmStatic
        @BindingAdapter("bind:imageUrl")
        fun loadImage(view: ImageView, imageUrl: String?) {
            Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .into(view)
        }
    }

    val planetaryResponse: LiveData<PlanetaryResponse>? = homeRespository?.getPlanetaryResponse()

    fun fetchPlanetary() {
         homeRespository?.fetchPlanetaryData()!!
    }

}